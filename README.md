# Random Sequence Frontend

This simple react application generates a random sequence of given length calling a serverless lambda function hosted on EC2 via an API endpoint. 

## Usage

First istall the required dependencies 
```
npm install 
```

then run the application
```
npm start
```

A new tab in your browser will appear. If it won't, simply open yourself a new one and type `http://localhost:3000/`.

## Screenshot

![](assets/screenshot.png)