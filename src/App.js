import React, { useState } from "react";
import { Row, Col, ListGroup, Button, Container, Toast, ToastContainer } from 'react-bootstrap';
import { MDBInput } from 'mdbreact';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css'

function App() {
  const [n, setN] = useState(0);
  const [randomValues, setRandomValues] = useState([]);
  const [message, setMessage] = useState('');

  /**
   * Fetches data from the API endpoint
   * @returns 
   */
  const fetchData = () => {
    const APIendpoint = `https://iqic74ux66.execute-api.us-east-2.amazonaws.com/default/random_number?n=${n}`;

    // for safety 
    if (isNaN(n) || n <= 0 ) {
      setMessage('Select a positive number');
      return;
    }

    setN(parseInt(n));

    fetch(APIendpoint)
      .then (res => res.json())
      .then ( result => setRandomValues(result),
              error =>  setMessage(error)
      );
  }

  const check_set_N = (val) => {
    if(val >= 0 && val <= Number.MAX_SAFE_INTEGER)
      setN(val);
  }

  return (
    <Container fluid>  
      <h3>How many values you want to generate (between 0 and 9):</h3> <br />
        <Row className="mx-auto">
          <Col xs={2}>
            <MDBInput type="number" value={n} onChange={ev => check_set_N(ev.target.value)}/>
          </Col>
        
          <Col xs={2}>
            <Button variant="primary" onClick={fetchData}> Generate </Button>
          </Col>
        </Row>
    

      <br></br>
      { randomValues.length > 0 && 
        <Row className='mx-auto'>
          <h5 className='mx-auto'>Random generated numbers: </h5>
        
          <ListGroup as='ul'>
            {randomValues.map( number => 
                <ListGroup.Item as="li" className='border-0'>{number}</ListGroup.Item>
            )}
          </ListGroup>
        </Row>
      }

      <ToastContainer position='top-end' className="p-2">
        <Toast  
          className="bg-primary" 
          show={message !== ''} 
          onClose={() => setMessage('')} 
          delay={2500} 
          animation
          autohide
          >
          <Toast.Header>Random generator says</Toast.Header>
          <Toast.Body>{message}</Toast.Body>
        </Toast>
      </ToastContainer>
    
    </Container>
  );

}

export default App;
